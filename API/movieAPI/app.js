var express=require("express"),
    app=express(),
    request=require("request"),
    bodyParser=require("body-parser");    
    
    
app.use(bodyParser.urlencoded({extended:true}));
// use public folder for aquireing css files
app.use(express.static("public"));
//set ejs as default render files
app.set("view engine","ejs");

app.post("/post",function(req,res)

{
    var title=req.body.movie;
    var url="http://www.omdbapi.com/?s="+title+"&apikey=thewdb"

    //routing

    request(url,function(error,response,body)
    { 
    
       if(response.statusCode==200)
       {
        //convert string in body to json
       var dat=JSON.parse(body);
          res.render("home",{data:dat});
        }
}
);
});
    

app.get("/search",function(req,res){res.render("search")});
//routing

//Make express to listen for the requests 
app.listen(process.env.PORT,process.env.IP,function(){console.log("Server of modula Working!");});