var express=require("express");
var app=express();

// use public folder for aquireing css files
app.use(express.static("public"));
//set ejs as default render files
app.set("view engine","ejs");

//routing
app.get("/",function(req,res){res.render("home")});



//Make express to listen for the requests 
app.listen(process.env.PORT,process.env.IP,function(){console.log("Server of modula Working!");})
