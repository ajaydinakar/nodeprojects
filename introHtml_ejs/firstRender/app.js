var express=require("express");
var app=express();
app.use(express.static("public"));

//get route to diplay the response of the url by html data in send  /
app.get("/inside",function(req,res){res.send("<h1>Hello World</h1><h2> this is sample html response </h2>")});

app.get("/home",function(req,res){res.render("home.ejs")});

//render route to diplay html page which is in views directory ,
app.get("/home",function(req,res){res.render("home.ejs")});

app.get("/profile/:name",function(req,res)
{
    var name=req.params.name;
    res.render("profile.ejs",{profilename:name})
});


//Make express to listen for the requests 
app.listen(process.env.PORT,process.env.IP,function(){console.log("Server Working!");})
