admin:~/environment $ mkdir introHtml_ejs
admin:~/environment $ cd introHtml_ejs/
admin:~/environment/introHtml_ejs $ mkdir firstRender
admin:~/environment/introHtml_ejs $ cd firstRender/
admin:~/environment/introHtml_ejs/firstRender $ ll
total 0
admin:~/environment/introHtml_ejs/firstRender $ touch app.js
admin:~/environment/introHtml_ejs/firstRender $ npm init
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg> --save` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
name: (firstRender) 
Sorry, name can no longer contain capital letters.
name: (firstRender) introejs
version: (1.0.0) 
description: creating first ejs file
entry point: (app.js) 
test command: 
git repository: 
keywords: 
author: ajaydinakar
license: (ISC) 
About to write to /home/ec2-user/environment/introHtml_ejs/firstRender/package.json:

{
  "name": "introejs",
  "version": "1.0.0",
  "description": "creating first ejs file",
  "main": "app.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "ajaydinakar",
  "license": "ISC"
}


Is this ok? (yes) 
admin:~/environment/introHtml_ejs/firstRender $ npm install express --save
introejs@1.0.0 /home/ec2-user/environment/introHtml_ejs/firstRender
└─┬ express@4.16.2 
  ├─┬ accepts@1.3.4 
  │ ├─┬ mime-types@2.1.17 
  │ │ └── mime-db@1.30.0 
  │ └── negotiator@0.6.1 
  ├── array-flatten@1.1.1 
  ├─┬ body-parser@1.18.2 
  │ ├── bytes@3.0.0 
  │ ├─┬ http-errors@1.6.2 
  │ │ ├── depd@1.1.1 
  │ │ ├── inherits@2.0.3 
  │ │ └── setprototypeof@1.0.3 
  │ ├── iconv-lite@0.4.19 
  │ └── raw-body@2.3.2 
  ├── content-disposition@0.5.2 
  ├── content-type@1.0.4 
  ├── cookie@0.3.1 
  ├── cookie-signature@1.0.6 
  ├─┬ debug@2.6.9 
  │ └── ms@2.0.0 
  ├── depd@1.1.2 
  ├── encodeurl@1.0.2 
  ├── escape-html@1.0.3 
  ├── etag@1.8.1 
  ├─┬ finalhandler@1.1.0 
  │ └── unpipe@1.0.0 
  ├── fresh@0.5.2 
  ├── merge-descriptors@1.0.1 
  ├── methods@1.1.2 
  ├─┬ on-finished@2.3.0 
  │ └── ee-first@1.1.1 
  ├── parseurl@1.3.2 
  ├── path-to-regexp@0.1.7 
  ├─┬ proxy-addr@2.0.2 
  │ ├── forwarded@0.1.2 
  │ └── ipaddr.js@1.5.2 
  ├── qs@6.5.1 
  ├── range-parser@1.2.0 
  ├── safe-buffer@5.1.1 
  ├─┬ send@0.16.1 
  │ ├── destroy@1.0.4 
  │ └── mime@1.4.1 
  ├── serve-static@1.13.1 
  ├── setprototypeof@1.1.0 
  ├── statuses@1.3.1 
  ├─┬ type-is@1.6.15 
  │ └── media-typer@0.3.0 
  ├── utils-merge@1.0.1 
  └── vary@1.1.2 

npm WARN introejs@1.0.0 No repository field.



admin:~/environment/introHtml_ejs/firstRender $ touch home.ejs
admin:~/environment/introHtml_ejs/firstRender $ npm install ejs --save
introejs@1.0.0 /home/ec2-user/environment/introHtml_ejs/firstRender
└── ejs@2.5.7 
admin:~/environment/introHtml_ejs/firstRender $ mkdir views
admin:~/environment/introHtml_ejs/firstRender $ mv home.ejs views/


admin:~/environment/introHtml_ejs/firstRender $ node app.js
Server Working!