var express=require("express");
var app=express();
var bodyParser=require("body-parser");

 var fruits=["apple","banane","cranberry","deepberry","fructose","grape"];

app.use(bodyParser.urlencoded({extended:true}));
// use public folder for aquireing css files
app.use(express.static("public"));
//set ejs as default render files
app.set("view engine","ejs");



//get routing 
app.get("/",function(req,res){res.render("home")});

//post route
app.post("/newfruit", function(req,res)

{
 var newfrt=req.body.newfruit;
fruits.push(newfrt);
res.redirect("/fruits");
 
});


//get routing 
app.get("/fruits",function(req,res)
{

    
    res.render("fruits",{fruit:fruits});
    
});

//Make express to listen for the requests 
app.listen(process.env.PORT,process.env.IP,function(){console.log("Server of modula Working!");})
