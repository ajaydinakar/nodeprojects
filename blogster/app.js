
var express=require("express"),
app=express(),
bodyparser=require("body-parser"),
mongoose=require("mongoose");

//app config
mongoose.connect("mongodb://localhost/blogster-app");
app.set("view engine","ejs");
app.use(express.static("public"));
app.use(bodyparser.urlencoded({extended:true}));



//mongoose/model config
var blogSchema=new mongoose.Schema({
    title:String,cd
    image:String,
    body:String,
    created:{ type:Date,default:Date.now}
    
});
var Blog=mongoose.model("Blog",blogSchema);
Blog.create({
    
    title:"Test Blog",
    image:"https://images.unsplash.com/photo-1489924034176-2e678c29d4c6?auto=format&fit=crop&w=1951&q=80",
    body:"this is simply a first post"
});

//routes

app.get("/",function(req,res){ res.redirect("/blogs");});

app.get("/blogs",function(req,res)
{
    
    Blog.find({},function(err,blogs)
    {
    if(err)
        {console.log("error");} 
    else
        {res.render("index",{blogs:blogs});}
    });
});    
   
//title


//image

//body


//

app.listen(process.env.PORT,process.env.IP,function(){console.log("server starting");});