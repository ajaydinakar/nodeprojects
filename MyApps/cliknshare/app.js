var express=require("express");
var app=express();
app.use(express.static("public"));

var bodyParser=require("body-parser");//this is used for retriving data from form data during post route
var mongoose =require("mongoose");


app.set("view engine","ejs");


//connect to database named natpic
mongoose.connect("mongodb://localhost/natpic");
app.use(bodyParser.urlencoded({extended:true}));

//create a schema (structure of database elements)
var imgSchema=new mongoose.Schema(
    {
        name:String,
        url:String,
        about:String
    
    }
    );
    
    //create a model 
    var image=mongoose.model("image",imgSchema);

    
   
app.get("/landing",function(req,res){res.render("landing")});

app.get("/dashboard/new",function(req,res){res.render("new")});



//render route to diplay html page which is in views directory ,
app.get("/dashboard",function(req,res){
    image.find({},function(error,allpictures)
    {
        if(error) 
        {
      console.log(error);
        }
      else
      {
          console.log("retrived");
       res.render("dashboard",{imgs:allpictures});
      } 
    });
    
   });

app.post("/dashboard",function(req,res)
{
var newname=req.body.name;
var newimage=req.body.image;
var newabout=req.body.about;
var newimgs={name:newname,url:newimage,about:newabout};
image.create(newimgs,function(error,image)
    {
      if(error)
      console.log("error");
      else
      {
          console.log("success");
     res.redirect("/dashboard");
      }
    });

});

app.get("/dashboard/:pic",function(req,res)
{
image.findById(req.params.pic,function(error,info)
{
    
   
 if(error) 
 {
     console.log("error");
 }
 else { 
    //console.log(data);
     res.render("show",{data:info}) ;   
       }
}
);
});
//Make express to listen for the requests 
app.listen(process.env.PORT,process.env.IP,function(){console.log("Server Working!");})
