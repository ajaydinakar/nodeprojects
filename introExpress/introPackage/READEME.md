
console log

admin:~/environment/introExpress $ cd introPackage/
admin:~/environment/introExpress/introPackage $ npm init
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg> --save` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
name: (introPackage) 
Sorry, name can no longer contain capital letters.
name: (introPackage) packageintro
version: (1.0.0) 
description: trying to learn package json
entry point: (app.js) 
test command: 
git repository: 
keywords: 
author: ajaydinakar
license: (ISC) 
About to write to /home/ec2-user/environment/introExpress/introPackage/package.json:

{
  "name": "packageintro",
  "version": "1.0.0",
  "description": "trying to learn package json",
  "main": "app.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "ajaydinakar",
  "license": "ISC"
}


Is this ok? (yes) 
admin:~/environment/introExpress/introPackage $ ls
app.js  package.json
admin:~/environment/introExpress/introPackage $ npm install cat-me --save
packageintro@1.0.0 /home/ec2-user/environment/introExpress/introPackage
└─┬ cat-me@1.0.3 
  └─┬ yargs@3.32.0 
    ├── camelcase@2.1.1 
    ├─┬ cliui@3.2.0 
    │ ├─┬ strip-ansi@3.0.1 
    │ │ └── ansi-regex@2.1.1 
    │ └── wrap-ansi@2.1.0 
    ├── decamelize@1.2.0 
    ├─┬ os-locale@1.4.0 
    │ └─┬ lcid@1.0.0 
    │   └── invert-kv@1.0.0 
    ├─┬ string-width@1.0.2 
    │ ├── code-point-at@1.1.0 
    │ └─┬ is-fullwidth-code-point@1.0.0 
    │   └── number-is-nan@1.0.1 
    ├── window-size@0.1.4 
    └── y18n@3.2.1 

npm WARN packageintro@1.0.0 No repository field.
admin:~/environment/introExpress/introPackage $ toiuch READEME.md
bash: toiuch: command not found
admin:~/environment/introExpress/introPackage $ touch READEME.md